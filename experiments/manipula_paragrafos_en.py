#-*- coding:utf-8 -*-
import sys

def separate_lines(arq_in,arq_out):
	with  open(arq_in, "r") as file_in, open(arq_out, "w") as file_out:
		count = 0
		end_paragraph = False
		old_line = ''
		for line in file_in:
			if line.endswith(".\r\n") or line.endswith("'\r\n"): #.\r\n
				line = line.replace("\r\n","")				
				if end_paragraph == True:
					file_out.write(old_line+"\n")
				old_line = line
				end_paragraph = True
			else:
				
				if (len(line)>0) or (line != "\n"):
					line = line.replace("\r\n","\n")

					if end_paragraph == True and line.startswith(' '):
						#print old_line+" -- "+line
						file_out.write(old_line+"\n"+line)
					else:
						file_out.write(old_line+" "+line)
					end_paragraph = False
					old_line = ''
				#print line.split(" ")
		


def join_lines(arq_in,arq_out):
	old_line = ""
	
	with  open(arq_in, "r") as file_in,open(arq_out, "w") as file_out:
	    for line in file_in:
	    	line = line.replace("\n","")
	    	line = line.strip()
	        if ((not line.endswith(".")) and (not line.endswith("'"))):
	        	#line = line.replace("\n","")
	        	line = line.strip()
	        	old_line += line + " " 
	        else:
	        	file_out.write(old_line+ line+"\n\n")
	        	old_line = ""

# 	print '\n\nOLD',old_line
def main(file):


	new_file_name = file.split('.')[0] +"_sepate_paragraphs.txt" 
	#print new_file_name +"_final.txt"
	#join_lines(file,new_file_name+"_join.txt")
	separate_lines(file,new_file_name)
	join_lines(new_file_name,new_file_name.split('.')[0]+"_final.txt")

if __name__ == '__main__':
	if len(sys.argv) != 2:
	    print "O comando deve ser:"
	    print "python %s <arq>" % (sys.argv[0])
	    sys.exit(1)
	file = sys.argv[1]
	main(file)