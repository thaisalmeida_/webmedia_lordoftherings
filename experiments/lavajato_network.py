# -*- coding: utf-8 -*-
from math import *
import numpy as np
from math import sqrt
import re
import sys

def remove_stopwords(line):
	stopwords = ['de', 'a', 'o', 'que', 'e', 'do', 'da', 'em','um', 'para', 'com', 'não', 'uma', 'os', 'no', 'se', 'na',
		'por', 'mais', 'as', 'dos', 'como', 'mas', 'ao', 'ele',	'das', 'à', 'seu', 'sua', 'ou', 'quando', 'muito', 'nos', 
		'já', 'eu', 'também', 'só', 'pelo', 'pela', 'até', 'isso', 'ela', 'entre', 'depois', 'sem', 'mesmo', 'aos', 'seus',
		'quem', 'nas', 'me', 'esse', 'eles', 'você', 'essa', 'num',	'nem', 'suas', 'meu', 'às', 'minha', 'numa','pelos',
		'elas', 'qual', 'nós', 'lhe', 'deles', 'essas','esses', 'pelas', 'este', 'dele', 'tu', 'te', 'vocês', 'vos','lhes',
		'meus', 'minhas', 'teu', 'tua', 'teus', 'tuas', 'nosso', 'nossa', 'nossos', 'nossas', 'dela', 'delas', 'esta',
		'estes','estas', 'aquele', 'aquela', 'aqueles', 'aquelas', 'isto', 'aquilo','estou', 'está', 'estamos', 'estão',
		'estive', 'esteve', 'estivemos','estiveram', 'estava', 'estávamos', 'estavam', 'estivera', 'estivéramos', 'serei',
		'esteja', 'estejamos', 'estejam', 'estivesse', 'estivéssemos', 'estivessem','estiver', 'estivermos', 'foi',
		'estiverem', 'hei', 'há', 'havemos', 'hão', 'houve','houvemos', 'houveram', 'houvera', 'houvéramos', 'haja',
		'hajamos', 'hajam','houvesse', 'houvéssemos', 'houvessem', 'houver', 'houvermos', 'houverem', 'houverei', 'houverá',
		'houveremos', 'houverão', 'houveria', 'houveríamos', 'houveriam', 'sou', 'somos', 'são', 'era', 'éramos', 'eram', 'fui',
		'fomos', 'foram', 'fora', 'fôramos', 'seja', 'sejamos', 'sejam', 'fosse','fôssemos', 'fossem', 'for', 'formos', 'forem',
		'será', 'seremos', 'serão', 'seria', 'seríamos', 'seriam', 'tenho', 'tem', 'temos', 'tém', 'tinha', 'tínhamos', 'tinham',
		'tive', 'teve', 'tivemos', 'tiveram', 'tivera', 'tivéramos', 'tenha', 'tenhamos', 'tenham', 'tivesse', 'tivéssemos',
		'tivessem', 'tiver', 'tivermos', 'tiverem', 'terei', 'terá', 'teremos', 'terão', 'teria', 'teríamos', 'teriam']
	
	for i in stopwords:
		pattern = re.compile(r'\b'+ i.upper() + r'\b')
		if(re.search(pattern,line.upper()) != None):
			line = line.replace(i,"")	
	return line	


def find_word_line(line,key,line_number,words_hash):
	line = remove_punctuation(line)
	pattern = re.compile(r'\b'+ key + r'\b')
	if(re.search(pattern,line.upper()) != None):
		words_hash[key].add(line_number)


def generate_bigram(arq_in,entity_hash):
	words_hash = {}
	line_number =1
	with open(arq_in, "r") as file:
		for line in file:
			line = line.replace("\n","").strip()
			line = remove_punctuation(line)
			line = remove_stopwords(line)
			words = line.split(" ")
			for w in words:
				if(len(w) > 3):
					w = w.strip()
					words_hash[w.upper()] = set([])
			line_number +=1

		for key in words_hash.keys():
			file.seek(0, 0)
			line_number = 0
			for line in file:
				line_number+=1
				find_word_line(line,key,line_number,words_hash)
		
	set_bigrams = set([])
	for i in words_hash.keys():
		print(i," ",entity_hash[i])
		for j in entity_hash.keys():
			intersection = set([])		
			intersection = words_hash[i] & entity_hash[j]
			if(len(intersection) > 0):
				set_bigrams.add((i,j[0],len(intersection)))
	bigrams_list = []
	for e in set_bigrams:
		if ( ( (e[0],e[1],e[2]) not in bigrams_list) and ( (e[1],e[0],e[2]) not in bigrams_list) ):
			x = str(e[1])
			x = x.split("|")[0]
			print(str(e[0])+","+x+","+str(e[2]))
			bigrams_list.append(e)


def name_entity(line):
	line = line.replace("\n","").strip()
	line = line.split("|")[0]
	return line

def nickname_entities(line):
	line = line.replace("\n","").strip()
	words_line = line.split("|")
	for i in range(0,len(words_line)):
		words_line[i] = words_line[i].strip()
	return words_line


def remove_punctuation(line):
	punctuation = ['.', '/', ''', ''', '?', '!', '$', '%', '^', '&',\
	         '*', '(', ')', ' - ', '_', '+', '=', '@', ':','\\', ',',\
	         ';', '~', '`', '<', '>', '|', '[', ']', '{', '}','–', '“',\
	         '»', '«', '°', '’', '1', '2','3', '4', '5', '6', '7', '8', '9', '0']
	for i in punctuation:
		line = line.replace(i," ")#opa
	return line

def find_entity_line(line,entity_hash,key,line_number):
	nickname = nickname_entities(key[0])

	for i in nickname:
		line = remove_punctuation(line)
		pattern = re.compile(r'\b'+ i.lower() + r'\b')
		if(re.search(pattern,line.lower()) != None):#opa
			entity_hash[key].add(line_number)
			
def determine_vertex(arq_in,entity_hash):
	id_vertex = 0
	vertex_list = []
	#print("Id,Label")
	with open(arq_in, "r") as file:
		for line in file:
			entity = line.replace("\n","").strip()
			entity_hash[(entity,id_vertex)] = set([])						
			#print(str(id_vertex)+","+entity.split("|")[0])
			vertex_list.append((entity,id_vertex))
			id_vertex+=1
	return vertex_list

def no_duplicate_edge(set_edge):
	edge_list = []
	print("Source,Target,Type,Weight")
	for e in set_edge:
		if ( ( (e[0],e[1],e[2]) not in edge_list) and ( (e[1],e[0],e[2]) not in edge_list) ):
			print(str(e[0])+","+str(e[1])+","+"Undirected,"+str(e[2]))
			edge_list.append(e)
	return edge_list
		
def entity_occur_line(arq_in,entity_hash):
	with  open(arq_in, "r") as file:
		for key in entity_hash.keys():
			file.seek(0, 0)
			line_number = 0
			for line in file:
				line_number+=1
				find_entity_line(line,entity_hash,key,line_number)

		set_edge = set([])
		for i in entity_hash.keys():
			#print(i," ",entity_hash[i])
			for j in entity_hash.keys():
				intersection = set([])		
				if(i[0] != j[0]):
					intersection = entity_hash[i] & entity_hash[j]
					if(len(intersection) > 0):
						set_edge.add((i[1],j[1],len(intersection)))
						#no_duplicate_edge(set_edge,i[1],j[1],intersection)
		return set_edge


def create_adjascent_list(list_edges, list_vertex):
	adjascent_list = {}
	for v_index in list_vertex:
		adjascent_list[v_index] = set([])
		for e in list_edges:
			if (v_index == e[0]):
				adjascent_list[v_index].add(e[1])
			elif ( v_index == e[1]):
				adjascent_list[v_index].add(e[0])

	return adjascent_list

def calculate_local_cluster(list_edges, list_vertex):
	adjascent_list = create_adjascent_list(list_edges, list_vertex)
	local_cluster_coefficient = []
	for v in list_vertex:
		adjascent_connected = 0
		number_adjascent = len(adjascent_list[v])
		if(number_adjascent > 1):
			for v_adjascent in adjascent_list[v]:
				adjascent_connected += len(adjascent_list[v] & adjascent_list[v_adjascent])		
			adjascent_connected = adjascent_connected/2 #undirected graph, a-b = b-a
			local_cluster_coefficient.append(2*adjascent_connected/float(number_adjascent*(number_adjascent-1)))
		else:
			local_cluster_coefficient.append(0.0)
	for i in local_cluster_coefficient:
		print(i) 
		

def determine_max(lista):
	max_number = -1
	for i in lista:
		if i > max_number :
			max_number = i
	return max_number
			
def normalize_weighted_degree(lista):
	lista_normalized = []
	max_number = determine_max(lista)
	for i in lista:
		lista_normalized.append(i/max_number)

	return lista_normalized

def calulate_entropy(arq_in):
	list_file, list_normalized, list_probabilities = [], [], []
	sum_weight_degree,entropy  = 0.0, 0.0

	with  open(arq_in, "r") as file:
		for line in file:
			line.replace("\n","").strip()
			list_file.append(float(line.split(",")[2]))

	list_normalized = normalize_weighted_degree(list_file)
	for i in list_normalized:
		sum_weight_degree += i
	for i in list_normalized:
		list_probabilities.append(i/sum_weight_degree)
	for pi in list_probabilities:
		entropy +=  pi * log(pi, 2)
		print (pi * log(pi, 2) *(-1))

	#print(entropy*(-1))




def main(file_characters,file_book):

	#calulate_entropy("wdegree.csv")	
	
	entity_hash = {}
	vertex = determine_vertex(file_characters,entity_hash)
	edges = entity_occur_line(file_book,entity_hash)
	lista = no_duplicate_edge(edges)

	vertex_alone = []
	for v in vertex:
		ok = False
		for e in lista:
			if v[1]== e[0] or v[1] == e[1]:
				ok = True
				break

		if ok == False:
			vertex_alone.append(v[1])

	#print("Id,Label")
	lista_vertex = []
	for v in vertex:
		if v[1] not in vertex_alone:
			label = str(v[0])
			label = label.split("|")[0]
			lista_vertex.append(v[1])
			#print(str(v[1])+","+label.strip())

	# lista_vertex = []
	# with  open("id.csv", "r") as file:
	# 	for line in file:
	# 		line.replace("\n","").strip()
	# 		v = int(line)
	# 		lista_vertex.append(v)


	# calculate_local_cluster(lista,lista_vertex)


if __name__ == '__main__':
	if len(sys.argv) != 3:
	    print "O comando deve ser:"
	    print "python %s <arq> <arq>" % (sys.argv[0])
	    sys.exit(1)
	file_characters = sys.argv[1]
	file_book = sys.argv[2]
	main(file_characters,file_book)