#-*- coding:utf-8 -*-
import sys

def controi_hash(arq_in):
	hash_id = {}
	line_nro = 0
	with  open(arq_in, "r") as file_in:
		for line in file_in:
			if line_nro > 0:
				line = line.replace("\n","")
				line = line.strip()
				line = line.split(',')
				hash_id[int(line[0])] = line[1]		
			line_nro += 1
	return hash_id

def substitui_id(hash_id,arq_in):
	line_nro = 0
	with  open(arq_in, "r") as file_in,open("edges_new.csv", "w") as file_out:
		file_out.write('Source,Target,Weight\n')
		for line in file_in:
			if line_nro > 0:
				line = line.replace("\n","")
				line = line.strip()
				line = line.split(',')
				file_out.write(hash_id[int(line[0])]+","+
							   hash_id[int(line[1])]+","+
							   line[2]+"\n")
			line_nro += 1


def main(file_vertex,file_edges):
	hash_id = controi_hash(file_vertex)
	substitui_id(hash_id,file_edges)


if __name__ == '__main__':
	if len(sys.argv) != 3:
	    print "O comando deve ser:"
	    print "python %s <arq> <arq>" % (sys.argv[0])
	    sys.exit(1)
	file_v = sys.argv[1]
	file_e = sys.argv[2]
	main(file_v,file_e)