# -*- coding: utf-8 -*-
from graph_tool.all import *
from math import log
import sys

def find_vertex(graph,vertex_id):
	for v in graph.vertices():
		if(graph.vp.vertex_id[v] == vertex_id):
			return v
	return None

def determine_vertex(arq_in, graph):
	with open(arq_in, "r") as file:
		for line in file:
			line = line.replace("\n","").strip()
			vertex_id = int(line.split(',')[0])
			vertex_label = line.split(',')[1]

			vertex = graph.add_vertex()
			graph.vp.vertex_id[vertex] = vertex_id
			graph.vp.vertex_label[vertex] = vertex_label 

def determine_eges(arq_in, graph):
	with open(arq_in, "r") as file:
		for line in file:
			line = line.replace("\n","").strip()
			line = line.split(',')
			vertice1_id = int(line[0])
			vertice2_id = int(line[1])
			edge_weight = float(line[2])
			v1 = find_vertex(graph,vertice1_id)
			v2 = find_vertex(graph,vertice2_id)
			if(v1 != None and v2 != None):
				e = graph.add_edge(v1,v2)
				graph.ep.edge_weight[e] = edge_weight 


def determine_max(graph, measure):
	max_number = -1
	for v in graph.vp[measure]:
		if v > max_number :
			max_number = v
	return max_number

def normalize_measure_edges(file_edges):
	list_file = []
	with open(file_edges, "r") as file_edges:
		for line in file_edges:
			line.replace("\n","").strip()
			list_file.append(float(line.split(",")[2]))
	sum_values = 0.0
	mean = 0.0
	
	for weight in list_file:
		sum_values += weight
	mean = sum_values/len(list_file)
	with open("edges_normalized.txt", "w") as file_out:
		for weight in list_file:
				file_out.write(str(weight/mean)+"\n")
			
def normalize_measure(graph, measure):
	# lista_normalized = []
	# max_number = determine_max(graph, measure)
	# for v in graph.vertices():
	# 		graph.vp[measure][v] = graph.vp[measure][v]/max_number
	sum_values = 0.0
	for v in graph.vertices():
		sum_values +=graph.vp[measure][v]
		
	for v in graph.vertices():
			graph.vp[measure][v] = graph.vp[measure][v]/sum_values
				
def store_centrality_measures(graph):
	file_out = open('centrality_measures_new.csv','w')
	file_out.write('Id,Label,Betweenness Centrality,Weighted Degree,Pagerank'+'\n')
	
	for v in graph.vertices():
		#print(str(graph.vp.vertex_id[v])+' '+str(graph.vp.eigenvector[v]))
		line = str(graph.vp.vertex_id[v])+','+graph.vp.vertex_label[v]+','+\
				str(graph.vp.betweenness[v])+','+str(graph.vp.wdegree[v])+','+\
				str(graph.vp.pagerank[v])
		file_out.write(line+'\n')
	file_out.close()

def calulate_entropy(graph, measure):
	list_probabilities = []
	sum_values,entropy  = 0.0, 0.0
	number_vertices = len(list(graph.vertices()))

	for v in graph.vp[measure]:
		sum_values += v
	
	for v in graph.vp[measure]:
		list_probabilities.append(v/sum_values)
	
	for pi in list_probabilities:
		if pi > 0:
			entropy +=  pi * log(pi, 2)
	print(measure.upper())
	print(entropy/log(number_vertices,2)*(-1))



def create_adjascent_list(list_edges, list_vertex):

	adjascent_list = {}
	for v_index in list_vertex:
		adjascent_list[v_index] = set([])
		for e in list_edges:
			if (v_index == e[0]):
				adjascent_list[v_index].add(e[1])
			elif ( v_index == e[1]):
				adjascent_list[v_index].add(e[0])

	return adjascent_list

def extract_vertex_edges(arq_edges, arq_vertex):
	list_edges,list_vertex = [], []
	
	with  open(arq_edges, "r") as file:
		for line in file:
			line.replace("\n","").strip()
			line = line.split(',')
			list_edges.append((line[0],line[1]))
	
	with  open(arq_vertex, "r") as file:
		for line in file:
			line.replace("\n","").strip()
			line = line.split(',')
			list_vertex.append(line[0])
	return list_vertex,list_edges


def calculate_local_cluster(arq_edges, arq_vertex,graph):
	list_vertex, list_edges = extract_vertex_edges(arq_edges, arq_vertex)
	adjascent_list = create_adjascent_list(list_edges, list_vertex)

	local_cluster_coefficient = []
	i = 0
	for v in list_vertex:
		adjascent_connected = 0
		number_adjascent = len(adjascent_list[v])
		if(number_adjascent > 1):
			for v_adjascent in adjascent_list[v]:
				adjascent_connected += len(adjascent_list[v] & adjascent_list[v_adjascent])		
			adjascent_connected = adjascent_connected/2 #undirected graph, a-b = b-a
			local_cluster_coefficient.append(2*adjascent_connected/float(number_adjascent*(number_adjascent-1)))
		else:
			local_cluster_coefficient.append(0.0)
	
	for v in graph.vertices():
		graph.vp.lcluster[v] = local_cluster_coefficient[i]
		i += 1
		

def get_wdregree(graph,file):
	list_file = []
	with open(file, "r") as file:
		for line in file:
			line.replace("\n","").strip()
			list_file.append(float(line.split(",")[2]))
	i = 0
	for v in graph.vertices():
		graph.vp.wdegree[v] = list_file[i]
		i+=1


def calculate_important_nodes(file):
	colums = []
	number_line = 1
	with open(file_vertex, "r") as file:
		for line in file:
			if number_line > 1:
				line.replace("\n","").strip()
				metrics = line.split(",")
				colums.append((float(metrics[2]),float(metrics[3]),float(metrics[4])))
			number_line+=1

	for i in colums:
		importance = pow(i[0]*i[1]*i[2],1/3.0)
		print importance


def main(file_vertex, file_edges):

	g = Graph(directed=False)

	#Initializing the graph...
	g.vp.vertex_label = g.new_vertex_property("string")
	g.vp.vertex_id = g.new_vertex_property("int")
	#determine_vertex(file_vertex, g)

	#g.ep.edge_weight = g.new_edge_property("double")
	#determine_eges(file_edges, g)
	
	#Calculating the centrality measures....
	#g.vp.betweenness = g.new_vertex_property("double")
	# g.vp.closeness = g.new_vertex_property("double")
	# g.vp.eigenvector = g.new_vertex_property("double")
	#g.vp.pagerank = g.new_vertex_property("double")
	#g.vp.wdegree = g.new_vertex_property("double")
	# g.vp.lcluster = g.new_vertex_property("double")
	# g.vp.eigentrust = g.new_vertex_property("double")

	#graph_tool.centrality.betweenness(g,vprop = g.vp.betweenness, weight = g.ep.edge_weight,norm = False)
	#graph_tool.centrality.closeness(g,vprop = g.vp.closeness, weight = g.ep.edge_weight,norm = False)
	#graph_tool.centrality.pagerank(g,prop = g.vp.pagerank, weight = g.ep.edge_weight)
	# #graph_tool.centrality.eigentrust(g, vprop = g.vp.eigentrust, trust_map = g.ep.edge_weight)

	# #largest_eigenvalue, g.vp.eigenvector = graph_tool.centrality.eigenvector(g, weight = g.ep.edge_weight,max_iter=100)
	#get_wdregree(g,file_vertex)
	#calculate_local_cluster(file_edges,file_vertex,g)

	# # calulate_entropy(g,'betweenness')
	# # calulate_entropy(g,'closeness')
	# # calulate_entropy(g,'eigenvector')
	# # calulate_entropy(g,'eigentrust')
	# # calulate_entropy(g,'pagerank')
	# # calulate_entropy(g,'lcluster')
	# # calulate_entropy(g,'wdegree')
	
	# # normalize_measure(g,'betweenness')
	# #normalize_measure(g,'closeness')
	# #normalize_measure(g,'eigenvector')
	# #normalize_measure(g,'eigentrust')
	# # normalize_measure(g,'pagerank')
	# #normalize_measure(g,'lcluster')
	# # normalize_measure(g,'wdegree')
	# #Storing the values....
	
	# normalize_measure_edges(file_edges)
	# store_centrality_measures(g)

	calculate_important_nodes(file_vertex)

if __name__ == '__main__':
	if len(sys.argv) != 3:
	    print "O comando deve ser:"
	    print "python %s <arq_vertex> <arq_edges>" % (sys.argv[0])
	    sys.exit(1)
	file_vertex = sys.argv[1]
	file_edges = sys.argv[2]
	main(file_vertex,file_edges)